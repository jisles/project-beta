from django.db import migrations


statuses = ["SCHEDULED", "CANCELED", "FINISHED"]


def up(apps, schema):
    Status = apps.get_model("service_rest", "Status")
    for id, status in enumerate(statuses):
        Status.objects.create(
            id=id,
            name=status,
        )


def down(apps, schema):
    Status = apps.get_model("service_rest", "Status")
    Status.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        (
            "service_rest",
            "0002_status_alter_appointment_options_appointment_vip_and_more",
        ),
    ]

    operations = [migrations.RunPython(up, down)]
