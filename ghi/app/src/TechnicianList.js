import React, { useEffect, useState } from "react"

function TechnicianList() {
  const [technicians, setTechnicians] = useState([])

  async function loadTechnicians() {
    const response = await fetch("http://localhost:8080/api/technicians");
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  useEffect(() => {
    loadTechnicians();
  }, []);

  async function handleDeleteTechnician(href) {
    const technicianUrl = `http://localhost:8080${href}`
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const technicianResponse = await fetch(technicianUrl, fetchOptions);
    if (technicianResponse.ok) {
      loadTechnicians();
    }
  }

  return (
    <div className="container my-5">
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {technicians && technicians.map(technician => {
            return (
              <tr className="table-info" key={technician.href} value={technician.href}>
                <td>{technician.employee_id}</td>
                <td>{technician.first_name}</td>
                <td>{technician.last_name}</td>
                <td>
                  <button className="btn btn-sm btn-primary"
                    onClick={() => handleDeleteTechnician(technician.href)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )

}

export default TechnicianList
