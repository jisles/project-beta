import React, { useEffect, useState } from "react";

function ModelList() {
  const [models, setModels] = useState([])

  async function loadModels() {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
      setModels(data.models)
    }
  }

  useEffect(() => {
    loadModels();
  }, []);

  async function handleDeletemodels(href) {
    const manufacturerUrl = `http://localhost:8100${href}`
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const modelResponse = await fetch(manufacturerUrl, fetchOptions);
    if (modelResponse.ok) {
      loadModels();
    }
  }

  return (
    <div className="container my-5">
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th>Name</th>
            <th>Manufacturer</th>
            <th>picture</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {models && models.map(models => {
            return (
              <tr className="table-info" key={models.href} value={models.href}>
                <td>{models.name}</td>
                <td>{models.manufacturer.name}</td>
                <td>
                  <img src={models.picture_url} alt="Car" style={{ maxWidth: '200px', maxHeight: '200px' }} />
                </td>
                <td>
                  <button className="btn btn-sm btn-primary"
                    onClick={() => handleDeletemodels(models.href)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )



}

export default ModelList
